package exam.challenge.bowling;

import java.util.Collection;
import java.util.LinkedHashSet;

public class Frame {
	
	public static final Integer MAX_FRAMES = 10;

	private Integer number;
	private Collection<PlayTurn> playTurns;

	public Frame(Integer number) {
		this.number = number;
	}

	public Integer getNumber() {
		return number;
	}

	public Collection<PlayTurn> getPlayTurns() {
		return playTurns;
	}

	public void addPlayTurn(PlayTurn playTurn) {
		if(this.playTurns == null) {
			this.playTurns = new LinkedHashSet<>();
		}
		this.playTurns.add(playTurn);
	}
	
	
	
	
	
}
