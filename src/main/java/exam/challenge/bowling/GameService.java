package exam.challenge.bowling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GameService {

	@Value("${participants.number}")
	private Integer participantsNumber;

	Collection<Frame> loadGame(String filePath) throws IOException {
		Collection<Frame> game = new LinkedHashSet<>();
		
		Path path = Paths.get(filePath);
		Stream<String> lines = Files.lines(path);
		
		String firstPlayer = null;
		String previousPlayer = null;

		PlayTurn currentPlayTurn = null;
		Frame currentFrame = null;
		Integer frameNumber = 0;
		Roll roll = null;
		
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		String line;
		while (Objects.nonNull(line = reader.readLine())) {  // each line from file represents a roll
			String[] fields = line.split("\\s");
			String currentPlayer = fields[0];
			roll = new Roll(fields[1]);
			if(Objects.isNull(firstPlayer)) { // the very first player? mark it
				firstPlayer = currentPlayer;
			}
			if(! currentPlayer.equals(previousPlayer)) { // new player -> new turn
				currentPlayTurn = new PlayTurn(currentPlayer);
				if(currentPlayer.equals(firstPlayer)) { // new player is the first player again -> new frame
					currentFrame = new Frame(++frameNumber);
				}
			}
			
			currentPlayTurn.addRoll(roll);
			currentFrame.addPlayTurn(currentPlayTurn);
			game.add(currentFrame);
			
			previousPlayer = currentPlayer;
		}
		
		reader.close();
		
		return game;
	}
	
	
	public Collection<String> getPlayers(Collection<Frame> game){
		Frame frame =  game.iterator().next();
		Set<String> players = frame.getPlayTurns().stream().map(
			turn -> {
				return turn.getPlayer();
			}
		).collect(Collectors.toSet());
		return players;
	}
	

	void printHello() {
		System.out.println("Hello from SomeService bean, participantsNumber: " + participantsNumber);
	}

}
