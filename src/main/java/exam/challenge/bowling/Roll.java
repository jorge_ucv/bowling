package exam.challenge.bowling;

public class Roll {
	
	public static final Integer MAX_HITS = 10;
	public static final Integer MIN_HITS = 0;
	
	private Boolean isFault;
	
	private Integer hits;
	
	
	public Roll(String numberOfHits) {
		if(numberOfHits == null ) {
			throw new InvalidBowlingDataException("knocked down pins value cannot be empty");
		}
		if(numberOfHits.toUpperCase().contentEquals("F")) {
			this.isFault = true;
			numberOfHits = "0";
		}
		Integer hits = Integer.parseInt( numberOfHits );
		if(hits < 0) {
			throw new InvalidBowlingDataException("the number of knocked down pins cannot be a negative value");
		}
		if(hits > 10) {
			throw new InvalidBowlingDataException("the number of knocked down pins cannot be more than ten");
		}
		this.hits = hits;
	}
	
	
	String getVisualResult(){
		return null;
	}

	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}


	public Boolean getIsFault() {
		return isFault;
	}


	public void setIsFault(Boolean isFault) {
		this.isFault = isFault;
	}
	
	
}
