package exam.challenge.bowling;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BowlingApplication implements CommandLineRunner {

	@Autowired
	private GameService gameService;

	public static void main(String[] args) {
		SpringApplication.run(BowlingApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Collection<Frame> game = gameService.loadGame(args[0]);
		
		Collection<String> players =  gameService.getPlayers(game);
		
		
		String headerLine = "Frame\t\t" + game.stream().map(f -> f.getNumber().toString() ).collect(Collectors.joining("\t\t") );
		System.out.println(headerLine);
		
		
		Map<String, Map<String,StringBuffer>> playerPrintMaps = new TreeMap<>();
		players.forEach(player -> {
			Map<String,StringBuffer> playerPrintMap = new TreeMap<>();
			playerPrintMap.put("name", new StringBuffer(player));
			playerPrintMap.put("pinfalls", new StringBuffer("Pinfalls"));
			playerPrintMap.put("score", new StringBuffer("Score"));
			playerPrintMaps.put(player, playerPrintMap);
		});
		for (Iterator<Frame> frameIterator = game.iterator(); frameIterator.hasNext();) {
			Frame frame = frameIterator.next();
			for(Iterator<PlayTurn> playTurnIterator = frame.getPlayTurns().iterator(); playTurnIterator.hasNext();) {
				PlayTurn  playTurn = playTurnIterator.next();
				playerPrintMaps.get(playTurn.getPlayer()).get("pinfalls").append(playTurn.getPinfall());
				playerPrintMaps.get(playTurn.getPlayer()).get("score").append(playTurn.getScore());
			}
		}
		
		players.forEach(player -> {
			System.out.println(playerPrintMaps.get(player).get("name"));
			System.out.println(playerPrintMaps.get(player).get("pinfalls"));
			System.out.println(playerPrintMaps.get(player).get("score"));
		});
		
		
	}

}
