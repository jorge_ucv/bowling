package exam.challenge.bowling;

import java.util.ArrayList;
import java.util.List;

public class PlayTurn {
	private String player;
	List<Roll> rolls;
	
	public PlayTurn(String player) {
		this.player = player;
	}
	
	public void addRoll(Roll roll) {
		if(this.rolls == null) {
			this.rolls = new ArrayList<>();
		}
		this.rolls.add(roll);
	}
	
	public String getPinfall() {
		return getPinfall("\t");
	}
	
	public String getPinfall(String valueSeparatorString) {
		
		Integer a = rolls.get(0).getHits();
		if(a == 10) {
			return valueSeparatorString+valueSeparatorString+"X";
		}
		Integer b = rolls.get(1).getHits();
		
		
		
		if(a + b < 10) {
			return valueSeparatorString + a + valueSeparatorString + b; 
		}
		
		return valueSeparatorString + a + valueSeparatorString + "/";
	}
	
	
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	
	
	public String getScore() {
		return getScore("\t");
	}
	
	public String getScore(String valueSeparatorString) {
		return valueSeparatorString + valueSeparatorString + "0";
	}
	
	
	
	
}
