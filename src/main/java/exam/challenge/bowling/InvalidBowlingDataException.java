package exam.challenge.bowling;

public class InvalidBowlingDataException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	public InvalidBowlingDataException() {
	}
	
	public InvalidBowlingDataException(String message) {
		super(message);
	}
	
}
